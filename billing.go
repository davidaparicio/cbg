package main

import (
	"fmt"
	"log"
	"strings"

	tokenizer "github.com/samber/go-gpt-3-encoder"
	openai "github.com/sashabaranov/go-openai"
)

type (
	tokenPrice struct {
		tokenType  string
		tokenPrice float64
	}
)

var priceList = []tokenPrice{
	{openai.GPT3Dot5Turbo, 0.002},
	{openai.GPT4, 0.06},
	{openai.GPT432K, 0.12},
}

func getTokenPrices(raw bool, m int, pq []string) {
	encoder, err := tokenizer.NewEncoder()
	if err != nil {
		log.Fatal(err)
	}

	tab := "| model | tokens | price |\n|---|---|---|\n"

	for _, t := range priceList {
		var (
			price, totalPrice float64
			totalTokens       int
		)

		// get tokens and price for prompt and query
		for i, str := range pq {
			encoded, err := encoder.Encode(str)
			if err != nil {
				log.Fatal(err)
			}
			// in gpt-4, price is / 2 for prompt (i == 0)
			if i == 0 && strings.HasPrefix(t.tokenType, "gpt-4") {
				price = t.tokenPrice / 2
			} else {
				price += t.tokenPrice
			}

			l := len(encoded)
			// do we have a max_token limit?
			if m != 0 && l > m {
				l = m
			}

			totalTokens += l
			totalPrice += (price * float64(l)) / 1000
		}

		tab = fmt.Sprintf("%s| %s | %d | %f |\n", tab, t.tokenType, totalTokens, totalPrice)
	}

	printOut(raw, tab)
}
