all:
	go build

install:
	go install

lint:
	golangci-lint run --verbose
